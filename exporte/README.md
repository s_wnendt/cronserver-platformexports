
# Exporte zu den Plattformen

- Lynn Exporte werden vom FTP Server geholt
- Exporte werden teilweise aufbereitet
- Exporte werden per Mail versendet oder auch auf externe FTP Server bereitgestellt

folgende Plattformen werden mit den automatisierten Exporten bedient:

- brokerbin
- PowerSourceOnline
- techbroker
- brokersite

crontab:

#powersource online
4 9 * * 1-5 /home/platformexports/./pso.sh

#delete brokerbin Exportfiles before export

40 8,11,14,17 * * 1-5 /home/platformexports/./del-brokerbinFilesFTP.sh

#brokerbin Exporte
0 9,12,15,18 * * 1-5 /home/platformexports/./brokerbin.sh

#getFiles for techbroker Mail and brokersite

3 9 * * 1-5 /home/platformexports/./getfilesfromftp.sh

#send Mail to brokersite

5 9 * * 1-5 /home/platformexports/./brokersitemail.sh

#send Mail to techbroker

6 9 * * 1-5 /home/platformexports/./techbrokermail.sh




