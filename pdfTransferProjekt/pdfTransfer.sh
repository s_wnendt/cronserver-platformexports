#!/bin/bash

RESULT=0
NC='\033[0m'
GREEN='\033[32m'
BLUE='\033[34m'
RED='\033[31m'

NC=''
GREEN=''
BLUE=''
RED=''

FILE_NUMBER=1
FILE='/home/platformexports/work/mount/wichtig.txt'
WORK='/home/platformexports/work'
MOUNT_DIR='/home/platformexports/work/mount'
SOURCE_DIR='/home/platformexports/work/mount'
TARGET_DIR='/home/platformexports/work/target'
ARCHIVE_DIR='/home/platformexports/work/archive'
CONTENT_DIR='/home/platformexports/work/content'
LOG_DIR='/home/platformexports/work/log'
ZIP_FILE_NAME="$(date +"%Y_%m_%d_%I_%M")"
LOG_FILE_NAME="${LOG_DIR}/${ZIP_FILE_NAME}.log"
NEW_ZIP_FILE_NAME=""
TEMP_ZIP_FILE_NAME="temp.zip"
MAX_ZIP_FILE_SIZE=102400000

log () {
    if [ -d $LOG_DIR ]; then
        printf "$@" >> $LOG_FILE_NAME
#printf "$@"
    fi
}

#ZIP-Datei per Mail versenden
send_eMail () {
    log "${GREEN}Die Datei wird per eMail versendet: ${NEW_ZIP_FILE_NAME}${NC}\n"
    # Zum Mailversand muss der User gewechselt werden. Da das Skript mit sudo aufgerufen wird, wird hier zu platformexports gewechselt.
    sudo -u platformexports mutt -e "set content_type=text/html" -s "Cybertrading invoices" "taxoffice@cybertrading.de" -b "a.linde@cybertrading.de" -a "${NEW_ZIP_FILE_NAME}" < "${CONTENT_DIR}/"content.html
}

error_finder () {
#Hiermit wird der rückgabe Wert jeder Function geprüft und das Programm bei Fehler beendet.
# -eq # equal
# -ne # not equal
# -lt # less than
# -le # less than or equal
# -gt # greater than
# -ge # greater than or equal


case $1 in
  0)
   ;;

  1)
    log "${RED}Das Programm wurde mit einem Fehlercode 1 Abgebrochen.${NC}\n"
    printf "Eine Fehler Mail wird versendet.\n"
    sudo -u platformexports mutt -e "set content_type=text/html" -s "Fehler bei 'Cybertrading invoices' aufgetreten" "a.linde@cybertrading.de" -b "s.wnendt@cybertrading.de" -a "${LOG_FILE_NAME}" < "${CONTENT_DIR}/"error.html

    # set -e ist ein Schalter, wenn er gesetzt ist und eine Function wird mit Returncode >0 beendet, so wird alles beendet.
    set -e
    return 1
    ;;

  2)
    log "${GREEN}Das Programm wurde beendet.${NC}\n"
    set -e
    return 1
    ;;

  3)
    # Ist noch nicht vergeben
    log "${RED}Das Programm wurde mit einem Fehlercode 3 beendet.${NC}\n"
    set -e
    return 1
    ;;

  *)
    log "${RED}Das Programm wurde mit einem Fehlercode $1 Abgebrochen (Unbekannt).${NC}\n"
    set -e
    return 1
    ;;
esac



#    if [ $1 -ne 0 ]
#    then
#        set -e
#        printf "${RED}Das Programm wurde mit einem Fehlercode $1 Abgebrochen.${NC}\n"
#
#        return 1
#    fi
}

set_directory () {
    if ! [ -d $1 ]; then
        mkdir $1
        log "${GREEN}Das Verzeichniss '${1}' musste angelegt werden.${NC}\n"
        if ! [ -d $1 ]; then
            log "${RED}Das Verzeichniss '${1}' konnte nicht angelegt werden.${NC}\n"

            return 1
        else
            log "${BLUE}Das Verzeichniss '${1}' konnte angelegt werden.${NC}\n"
        fi
    else
        log "${BLUE}Das Verzeichniss '${1}' ist vorhanden.${NC}\n"
    fi
}


set_directory_structure () {
    #Verzeichnissstruktur kontrillieren und wenn nötig anlegen.
    set_directory $WORK
    error_finder $?

    set_directory $LOG_DIR
    error_finder $?

    set_directory $MOUNT_DIR
    error_finder $?

    set_directory $TARGET_DIR
    error_finder $?

    set_directory $ARCHIVE_DIR
    error_finder $?

    set_directory $CONTENT_DIR
    error_finder $?
    return 0
}


#Gemountetes Laufwerk kontrollieren, wenn nötig neu mounten.
set_mount_directory () {
    if [ -f "$FILE" ]; then
        log "${BLUE}Auf das gemountete LW ${MOUNT_DIR} kann zugegriffen werden.${NC}\n"
    else
        log "${RED}Das LW scheint nicht gemountete zu sein, es muss neu gemountet werden.${NC}\n"
        mount -t cifs -o user=otto,domain=ct.local,vers=3.0,password='so9nT!cCpb' //172.22.54.23/Otto/Eingangsrechnung ${MOUNT_DIR}

        if [ -f "$FILE" ]; then
            log "${GREEN}Das LW ist jetzt gemountet.${NC}\n"
        else
            log "${RED}Das LW konnte nicht gemountet werden. ABBRUCH!!!.${NC}\n"

            return 1
        fi
    fi
    return 0
}


#Quelldateien in das Zieldateien kopieren
muve_file () {
    #Ist eine Datei im Verzeichniss?
    if ls ${MOUNT_DIR}/*.pdf 1> /dev/null 2>&1; then
        #ls -la ${MOUNT_DIR}/*.pdf
        mv ${MOUNT_DIR}/*.pdf ${TARGET_DIR}
   else
        log "${GREEN}Keine PDF-Dateien zum Verarbeiten vorhanden.${NC}\n"

        return 2
    fi
}

#Zippt die PDF-Dateien
zip_files () {
    ZIP_FILE_SIZE=0

    # Übergebender PDF-Dateiname
    log "Zu verarbeitende Datei: $1\n"

    # Name für Zip-Datei bilden
    NEW_ZIP_FILE_NAME="${ZIP_FILE_NAME}-${FILE_NUMBER}.zip"
    log "Benuzte ZIP-Datei: ${NEW_ZIP_FILE_NAME}\n"

    # Zippen der übergebenen Datei in temp.zip
    zip -j -q $TEMP_ZIP_FILE_NAME $1

    # Dateigröße von temp.zip ermitteln
    TEMP_FILE_SIZE=$(stat -c%s $TEMP_ZIP_FILE_NAME)
    log "Größe der temp.zip = ${TEMP_FILE_SIZE} in bytes.\n"

    # Dateigröße von 'NEW_ZIP_FILE_NAME' ermitteln, wenn Datei vorhanden....
    if [ -f "$NEW_ZIP_FILE_NAME" ]; then
        ZIP_FILE_SIZE=$(stat -c%s $NEW_ZIP_FILE_NAME)
    fi
    log "Größe der aktuellen ZIP-Datei: $NEW_ZIP_FILE_NAME = ${ZIP_FILE_SIZE} in bytes.\n"

    # ist die Gesammtgröße größer als erlaubt, neue ZIP-File anfangen.
    SUM_SIZE="$(($TEMP_FILE_SIZE+$ZIP_FILE_SIZE))"
    if [ $SUM_SIZE -gt $MAX_ZIP_FILE_SIZE ]
    then
        send_eMail
        error_finder $?

	FILE_NUMBER="$(($FILE_NUMBER+1))"
        NEW_ZIP_FILE_NAME="${ZIP_FILE_NAME}-${FILE_NUMBER}.zip"
        log "Es musste eine weitere ZIP-Datei erstellt werden: ${NEW_ZIP_FILE_NAME}\n"
    fi

    # -m dateien werden anschließend gelöscht
    # -q leise, ohne Ausgabe
    # -j ohne Pfadangabe

    # ansonnsten ZIP-File erweitern
    zip -j -q -m $NEW_ZIP_FILE_NAME $1

    # Ermitteln der Dateigröße der aktuelle ZIP-Datei
    ZIP_FILE_SIZE=$(stat -c%s $NEW_ZIP_FILE_NAME)
    log "Abschließende Größe der aktuellen ZIP-Datei: $NEW_ZIP_FILE_NAME = ${ZIP_FILE_SIZE} in bytes.\n\n"

    # temp.zip löschen
    rm $TEMP_ZIP_FILE_NAME
}

#Ließt Verzeichnis aus und übergibt namen zum Zippen weiter.
dir_list_and_zip () {
    cd ${TARGET_DIR}

    # Ließt alle Dateinamen aus mit der Ändung '.pdf'
    for FILE in $(ls | egrep -i "\.pdf$"); do  #Regex Filterung nach Dateiendung
        FILE=$(echo "${PWD}/${FILE}" | sed 's/ /\\ /g')
        zip_files "${FILE}" # Übergabe des Dateinamens zum zippen
    done

    send_eMail
    error_finder $?
}

# Löscht alle Dateien im Archive (*.zip) und LOG (*.log) Ordner die älter als 31 Tage sind.
clean_directory () {
    find ${ARCHIVE_DIR}/*.zip -mtime +31 -exec rm {} \;
    find ${LOG_DIR}/*.log -mtime +31 -exec rm {} \;
}

muve_to_archive () {
    cd ${TARGET_DIR}
    for FILE in $(ls | egrep -i "\.zip$"); do  #Regex Filterung nach Dateiendung
        FILE=$(echo "${PWD}/${FILE}" | sed 's/ /\\ /g')
        mv ${FILE} ${ARCHIVE_DIR}
    done
}

# Erstellt, wenn nicht vorhanden, die Ordnerstruktur
log "set_directory_structure=\n"
set_directory_structure
error_finder $?

# Mountet, wenn nicht verbunden, das Mount-LW
log "set_mount_directory=\n"
set_mount_directory
error_finder $?

# Löscht Dateien im Archiv
log "clean_directory=\n"
clean_directory
error_finder $?

# Verschiebt die Zip-Dateien des letzten Aufrufs in das Archiv
log "muve_to_archive=\n"
muve_to_archive
error_finder $?

# Verschiebt alle PDF-Dateien aus dem gemounteten Verzeichnis in das Target-Verzeichnis
log "muve_file=\n"
muve_file
error_finder $?

# Alle PDF-Dateien werden einzelnd gezippt
log "dir_list_and_zip=\n"
dir_list_and_zip
error_finder $?

log "Das Programm wurde ordnungsgemäß ausgeführt.\n"
return 0
